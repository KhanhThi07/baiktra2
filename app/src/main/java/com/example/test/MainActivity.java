package com.example.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lvPeople;
    ArrayList<People> arrayPeople;
    PeopleAdapter adapter;
    String ten;
    String sdt;
    String tich;
    int hinh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        AnhXa();
        adapter = new PeopleAdapter(this,R.layout.line_info,arrayPeople);
        lvPeople.setAdapter(adapter);
        registerForContextMenu(lvPeople);
        lvPeople.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                People pp = (People) arrayPeople.get(i);
                //Toast.makeText(MainActivity.this, pp.getSdt(), Toast.LENGTH_SHORT).show();*/
                Intent myIntent=new Intent(MainActivity.this, ChildActivity.class);
                myIntent.putExtra("Ten",pp.getTen());
                myIntent.putExtra("Sdt",pp.getSdt());
                myIntent.putExtra("Tich",pp.getSdt());
                myIntent.putExtra("Hinh",pp.getHinh());
                startActivity(myIntent);
            }
        });
    }

    private void AnhXa(){
        lvPeople =(ListView) findViewById(R.id.lv);

        arrayPeople = new ArrayList<>();
        arrayPeople.add(new People("VinMart","Chuỗi siêu thị uy tính. Sản phẩm" +
                "đa dạng. Vận chuyển siêu tốc",R.drawable.logovinmart));
        arrayPeople.add(new People("Meiji","Nhãn hiệu sữa bán chạy số 1 tại Nhât" +
                "Bản",R.drawable.logovinmart));
        arrayPeople.add(new People("Bác Tôm","Thực phẩm sạch đặc sản vùng miền",R.drawable.logovinmart));
        arrayPeople.add(new People("F5 Fruit","Nhà bán lẻ trái cây nhập khẩu",R.drawable.logovinmart));
        arrayPeople.add(new People("F5 Fruit","Nhà bán lẻ trái cây nhập khẩu",R.drawable.logovinmart));
        arrayPeople.add(new People("F5 Fruit","Nhà bán lẻ trái cây nhập khẩu",R.drawable.logovinmart));
        arrayPeople.add(new People("F5 Fruit","Nhà bán lẻ trái cây nhập khẩu",R.drawable.logovinmart));
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos=info.position;
        int id = item.getItemId();
        People sg=arrayPeople.get(pos);
        switch(id)
        {

            case R.id.ct_xoa:
                showDe(sg);

                break;

        }

        return super.onContextItemSelected(item);
    }
    public void showDe(final People name)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Bạn có muốn xóa không");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                arrayPeople.remove(name);
                adapter.notifyDataSetChanged();
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        builder.show();
    }



}